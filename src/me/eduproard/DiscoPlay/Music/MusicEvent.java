package me.eduproard.DiscoPlay.Music;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import me.eduproard.DiscoPlay.Main;
import net.md_5.bungee.api.ChatColor;

public class MusicEvent implements Listener {
	
	private Main Main;
	public MusicEvent(Main Plugin) {
		Main = Plugin;
		
		Main.getServer().getPluginManager().registerEvents(this, Main);
	}
	
	/*
	 * Inventory events
	 */
	
	@EventHandler
	public void on(InventoryClickEvent e) {
		if ((e.getWhoClicked() instanceof Player)) {
			Player p = (Player) e.getWhoClicked();
			
			if (e.getClickedInventory().getTitle().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', this.Main.getConfig().getString("HeadInventoryTitle")))) {
				e.setCancelled(true);
				return;
			}
			if (e.getClickedInventory().getTitle().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', this.Main.getConfig().getString("InventoryTitle")))) {
				e.setCancelled(true);
				ItemStack is = e.getCurrentItem();
				if (is.getType().isRecord()) {
					Material Record = is.getType();
					Location loc = (Location) this.Main.Inventory.get(p);
					Main.Players.get(loc).PlayRecord(p, Record);
					p.closeInventory();
					Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', this.Main.getConfig().getString("MUSIC_CHANGED").replaceAll("%player%", p.getName()).replaceAll("%song%", Main.getConfig().getString("InventoryRecords."+ is.getType().name().toUpperCase()+ ".RecordDisplayName"))));
				}
			}
		}
	}
	@EventHandler
	public void on(InventoryCloseEvent e) {
		if(e.getPlayer() instanceof Player) {
			Player p = (Player) e.getPlayer();
			if(e.getInventory().getTitle().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("InventoryTitle"))) ||
					e.getInventory().getTitle().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("HeadInventoryTitle")))) {
				if(Main.Inventory.containsKey(p)) {
					Main.Inventory.remove(p);
				}
				if(Main.InvTask.containsKey(p)) {
					Main.InvTask.get(p).cancel();
					Main.InvTask.remove(p);
				}
			}
		}
	}
	@EventHandler
	public void on(InventoryMoveItemEvent e) {
		if(e.getSource().getTitle().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("InventoryTitle")))) {
			e.setCancelled(true);
		} else if(e.getSource().getTitle().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("HeadInventoryTitle")))) {
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void on(InventoryDragEvent e) {
		if(e.getInventory().getTitle().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("InventoryTitle")))) {
			e.setCancelled(true);
		} else if(e.getInventory().getTitle().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("HeadInventoryTitle")))) {
			e.setCancelled(true);
		}
	}
	
	/*
	 * Jukebox events
	 */
	@EventHandler
	public void on(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Block b = e.getClickedBlock();
		
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_BLOCK) {
			if (b != null && b.getType() == Material.JUKEBOX) {
				Location loc = b.getLocation();
				if (Main.Players.containsKey(loc)) {
					e.setCancelled(true);
					if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
						if(Main.Players.get(loc).isPlayer() == true) {
							Main.Inventory.put(p, loc);
							p.openInventory(new MusicInventory(Main).getHeadInventory(p, loc));
						} else {
							if(p.hasPermission(new Permission("discoplay.select", PermissionDefault.OP)) == true) {
								Main.Inventory.put(p, loc);
								p.openInventory(new MusicInventory(Main).getInventory(p, loc));
							} else {
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO_PERMISSION_MUSIC")));
							}
						}
					}
				}
			}
		}
	}

	@EventHandler
	public void on(BlockBreakEvent e) {
		Block b = e.getBlock();
		if (b != null && b.getType().isRecord()) {
			Location loc = b.getLocation();
			if (Main.Players.containsKey(loc)) {
				e.setCancelled(true);
			}
		}
	}
}