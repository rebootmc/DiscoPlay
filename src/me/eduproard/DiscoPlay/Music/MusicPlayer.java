package me.eduproard.DiscoPlay.Music;

import org.bukkit.Material;
import org.bukkit.block.Jukebox;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import me.eduproard.DiscoPlay.Main;

public class MusicPlayer {
	
	private Jukebox Jukebox;
	
	private BukkitTask BukkitTask;
	private Main Main;
	
	private int DurationState;
	private boolean isPlayer;
	private Player Selector;
	
	public MusicPlayer(Main Plugin, Jukebox _Jukebox) {
		Main = Plugin;
		Jukebox = _Jukebox;
	}
	
	public void PlayRecord(Player p, Material Record) {
		if (Record.isRecord() == true) {
			Selector = p;
			isPlayer = true;
			((Jukebox)Jukebox.getBlock().getState()).setPlaying(Record);
			StartTask(Record);
		}
	}
	
	private void StartTask(Material Record) {
		MusicUtils Utils = new MusicUtils(Main);
		BukkitTask = new BukkitRunnable() {
			
			private MusicPlayer Player;
			private int Duration;
			
			public BukkitRunnable set(MusicPlayer MusicPlayer, int _Duration) {
				Player = MusicPlayer;
				Duration = _Duration;
				return this;
			}
			
			@Override
			public void run() {
				if(Player.getDurationState() == Duration) {
					this.cancel();
					Player.setPlayer(false);
					Player.setSelector(null);
					DurationState = 0;
				}
				Player.increaseDuration();
			}
		}.set(this, Utils.getDuration(Record)).runTaskTimer(Main, 0, 20);
	}
	
	public boolean isPlaying() {
		return ((Jukebox) Jukebox.getBlock().getState()).isPlaying();
	}
	public String getPlaying() {
		return ((Jukebox) Jukebox.getBlock().getState()).getPlaying().name().toUpperCase();
	}
	public Jukebox getJukebox() {
		return Jukebox;
	}
	public void setJukebox(Jukebox j) {
		Jukebox = j;
	}
	public BukkitTask getBukkitTask() {
		return BukkitTask;
	}
	public Player getSelector() {
		return Selector;
	}
	public void setSelector(Player p) {
		Selector = p;
	}
	public boolean isPlayer() {
		return isPlayer;
	}
	public void setPlayer(boolean isplayer) {
		isPlayer = isplayer;
	}
	public void increaseDuration() {
		DurationState++;
	}
	public int getDurationState() {
		return DurationState;
	}
}






