package me.eduproard.DiscoPlay.Music;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

import me.eduproard.DiscoPlay.Main;

public class MusicUtils {
	
	private Main Main;
	
	public MusicUtils(Main Plugin) {
		Main = Plugin;
	}
	/*
	 * MusicPlayer utils
	 */
	public int getDuration(Material Record) {
		String Duration = Main.getConfig().getString(Record.name().toUpperCase());
		
		int Minuts = Integer.parseInt(Duration.split("-")[0]);
		int Seconds = Integer.parseInt(Duration.split("-")[1]);
		
		int MinutsSec = Minuts * 60;
		return MinutsSec + Seconds;
	}

	
	/*
	 * Jukebox block utils
	 */
	public String LocationToString(Location Location, boolean YawPitch) {
		if(Location == null) {
			return null;
		}
		World W = Location.getWorld();
		int X = Location.getBlockX();
		int Y = Location.getBlockY();
		int Z = Location.getBlockZ();
		float YAW = Location.getYaw();
		float PITCH = Location.getPitch();
		
		StringBuilder Converted = new StringBuilder();
		Converted.append(W.getName() + "&" + X + "&" + Y + "&" + Z);
		if(YawPitch == true) {
			Converted.append("&" + YAW + "&" + PITCH);
		}
		return Converted.toString();
	}
	
	public Location StringToLocation(String Location, boolean YawPitch) {
		if(Location == null) {
			return null;
		}
		String[] Splitted = Location.split("&");
		
		World W = Bukkit.getWorld(Splitted[0]);
		int X = Integer.parseInt(Splitted[1]);
		int Y = Integer.parseInt(Splitted[2]);
		int Z = Integer.parseInt(Splitted[3]);
		float YAW;
		float PITCH;
		if(YawPitch == true) {
			YAW = Float.parseFloat(Splitted[4]);
			PITCH = Float.parseFloat(Splitted[5]);
			
			return new Location(W, X, Y, Z, YAW, PITCH);
		}
		return new Location(W, X, Y, Z);
		
	}
}