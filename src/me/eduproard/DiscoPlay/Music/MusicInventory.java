package me.eduproard.DiscoPlay.Music;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import me.eduproard.DiscoPlay.Main;

public class MusicInventory {
	
	
	private Main Main;
	
	public MusicInventory(Main Plugin) {
		Main = Plugin;
	}
	
	public Inventory getInventory(Player p, Location loc) {
		Inventory inv = Bukkit.createInventory(null, 27, ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("InventoryTitle")));
		int Slot = 0;
		for(String RecordString : Main.getConfig().getConfigurationSection("InventoryRecords").getKeys(false)) {
			ItemStack is = new ItemStack(Material.getMaterial(RecordString)); {
				if(Main.Players.get(loc).getPlaying().equals(RecordString)) {
					is.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1);
				}
				ItemMeta im = is.getItemMeta();
				im.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("InventoryRecords." + RecordString + ".RecordDisplayName")));
				List<String> prelore = new ArrayList<>();
				for(String line : Main.getConfig().getStringList("InventoryRecords." + RecordString + ".RecordLore")) {
					prelore.add(ChatColor.translateAlternateColorCodes('&', line));
				}
				im.setLore(prelore);
				im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
				is.setItemMeta(im);
			}
			inv.setItem(Slot, is);
			if(Slot == 8) {
				Slot = 12; 
			} else {
				Slot++;
			}
		}
		return inv;
	}
	public Inventory getHeadInventory(final Player p, final Location loc) {
		final Inventory inv = Bukkit.createInventory(null, 27, ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("HeadInventoryTitle")));
		final int Slot = 13;
		final MusicPlayer player = Main.Players.get(loc);
		final String mat = player.getPlaying();
		BukkitTask Task = new BukkitRunnable() {
			@Override
			public void run() {
				if (player.isPlayer() == true) {
					int Duration = player.getDurationState();
					Calendar c = Calendar.getInstance();
					c.setTimeInMillis(Duration * 1000);
					int minutes = c.get(Calendar.MINUTE);
					int seconds = c.get(Calendar.SECOND);
					ItemStack is = new ItemStack(Material.SKULL_ITEM); {
						SkullMeta im = (SkullMeta) is.getItemMeta();
						im.setOwner(player.getSelector().getName());
						List<String> prelore = new ArrayList<>();
						for(String line : Main.getConfig().getStringList("HeadLore")) {
							prelore.add(ChatColor.translateAlternateColorCodes('&', line.replaceAll("%song%", Main.getConfig().getString("InventoryRecords." + mat + ".RecordDisplayName")).replaceAll("%author%", Main.getConfig().getString("InventoryRecords." + mat + ".Author"))).replaceAll("%duration%", minutes + ":" + seconds));
						}
						im.setLore(prelore);
						im.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("HeadDisplayName").replaceAll("%player%", p.getName())));
						is.setItemMeta(im);
					}
					inv.setItem(Slot, is);
				} else {
					this.cancel();
					p.closeInventory();
				}
			}
		}.runTaskTimer(Main, 0, 20);
		Main.InvTask.put(p, Task);
		return inv;
	}
}