package me.eduproard.DiscoPlay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Jukebox;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import me.eduproard.DiscoPlay.Music.MusicEvent;
import me.eduproard.DiscoPlay.Music.MusicPlayer;
import me.eduproard.DiscoPlay.Music.MusicUtils;

public class Main extends JavaPlugin implements Listener {
	
	public Map<Location, MusicPlayer> Players = new HashMap<>();
	public Map<Player, Location> Inventory = new HashMap<>();
	public Map<Player, BukkitTask> InvTask = new HashMap<>();
	
	public void onEnable() {
		saveDefaultConfig();
		LoadPlayers();
		
		new MusicEvent(this);
		new jukeboxCMD(this);
		
		getLogger().info("ENABLED!");
	}
	public void onDisable() {
		SavePlayers();
		getLogger().info("DISABLED!");
	}
	
	
	private void LoadPlayers() {
		MusicUtils Utils = new MusicUtils(this);
		for(String loc : getConfig().getStringList("MusicPlayers")) {
			Location location = Utils.StringToLocation(loc, false);
			Block block = location.getBlock();
			if(block.getType() == Material.JUKEBOX) {
				Jukebox j = (Jukebox) block.getState();
				MusicPlayer player = new MusicPlayer(this, j);
				Players.put(location, player);
			}
		}
		getLogger().info("MusicPlayers loaded!");
	}
	private void SavePlayers() {
		MusicUtils Utils = new MusicUtils(this);
		List<String> musicplayers = new ArrayList<>();
		for(MusicPlayer player : Players.values()) {
			Jukebox j = player.getJukebox();
			Location loc = j.getBlock().getLocation();
			String location = Utils.LocationToString(loc, false);
			
			musicplayers.add(location);
		}
		getConfig().set("MusicPlayers", musicplayers);
		saveConfig();
		getLogger().info("MusicPlayers saved!");
	}
}