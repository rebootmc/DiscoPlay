package me.eduproard.DiscoPlay;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Jukebox;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.eduproard.DiscoPlay.Music.MusicPlayer;
import me.eduproard.DiscoPlay.Music.MusicUtils;
import net.md_5.bungee.api.ChatColor;

public class jukeboxCMD implements CommandExecutor {
	
	private Main Main;
	public jukeboxCMD(Main Plugin) {
		Main = Plugin;
		
		Main.getCommand("jukebox").setExecutor(this);
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command c, String label, String[] args) {
		
		if(!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "You can execute this command only in-game!");
			return false;
		}
		
		if(args.length == 0 || args.length >= 2) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("SYNTAX_ERROR")));
			return false;
		}
		Player p = (Player) sender;
		
		if(p.hasPermission("discoplay.cmds") == false) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("NO_PERMISSION")));
			return false;
		}
		
		if(args.length == 1) {
			String action = args[0];
			MusicUtils Utils = new MusicUtils(Main);
			Location loc = Utils.StringToLocation(Utils.LocationToString(p.getLocation().subtract(0, 1, 0), false), false);
			
			if(action.equalsIgnoreCase("set")) {
				
				
				if(Main.Players.containsKey(loc) == true) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("JUKEBOX_EXIST")));
					return false;
				}
				Block b = loc.getBlock();
				b.setType(Material.JUKEBOX);
				Jukebox j = (Jukebox) b.getState();
				MusicPlayer music = new MusicPlayer(Main, j);
				Main.Players.put(loc, music);
				//Main.Players.get(loc).PlayNext();
				
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("JUKEBOX_SET")));
			} else if(action.equalsIgnoreCase("remove")) {
				
				
				if(Main.Players.containsKey(loc) == false) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("JUKEBOX_NOT_EXIST")));
					return false;
				}
				
				if(Main.Players.get(loc).isPlayer()) {
					Main.Players.get(loc).getBukkitTask().cancel();
				}
				if(Main.Inventory.containsValue(loc)) {
					for(Player pl : Main.Inventory.keySet()) {
						if(Main.Inventory.get(pl) == loc) {
							pl.closeInventory();
						}
					}
				}
				Main.Players.remove(loc);
				
				Block b = loc.getBlock();
				b.setType(Material.AIR, true);
				
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("JUKEBOX_REMOVED")));
			} else {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getConfig().getString("SYNTAX_ERROR")));
			}
		}
		return false;
	}
}